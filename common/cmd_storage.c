/*
 * (C) Copyright 2011
 * Joe Hershberger, National Instruments, joe.hershberger@ni.com
 *
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <command.h>
#include <linux/ctype.h>

extern int set_current_storage_to_nand(void);
extern int set_current_storage_to_qspi(void);
extern void board_nand_init(void);

static int do_storage(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	char output[16];

	if (argc < 2 || strlen(argv[1]) != 4) /* nand or qspi */
		return CMD_RET_USAGE;

	output[0] = tolower(argv[1][0]);
	output[1] = tolower(argv[1][1]);
	output[2] = tolower(argv[1][2]);
	output[3] = tolower(argv[1][3]);

	if (strncmp(output, "nand", 4) == 0) {
		int ret = set_current_storage_to_nand();

		if (ret == 0) {
				board_nand_init();
				printf("Current storage device is nand\n");
		} else {
				printf("Select storage device error:%s\n", argv[1]);
				return 1;
		}
	} else if (strncmp(output, "qspi", 4) == 0) {
		int ret = set_current_storage_to_qspi();

		if (ret == 0) {
			/*ret = spi_flash_probe(0,0,1000000,3);
			if ( !ret ) {
				printf("Select storage device error:%s\n", argv[1]);
				return 1;
			} else {
				printf("Current storage device is qspi\n");
			}*/
		} else {
			printf("Select storage device error:%s\n", argv[1]);
			return 1;
		}
	} else {
			printf("Invalid paramater:%s\n", argv[1]);
			return 1;
	}

	return 0;
}

U_BOOT_CMD(
	storage, 2, 1, do_storage,
	"Select Nand or QSPI as the current storage device",
	"nand | qspi\n"
);
