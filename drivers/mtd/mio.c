#define PS7_MASK_POLL_TIME		100000000

#define OPCODE_EXIT			0
#define OPCODE_CLEAR			1
#define OPCODE_WRITE			2
#define OPCODE_MASKWRITE		3
#define OPCODE_MASKPOLL			4
#define OPCODE_MASKDELAY		5

#define NEW_PS7_ERR_CODE		1

/* Encode number of arguments in last nibble */
#define EMIT_EXIT()			((OPCODE_EXIT << 4) | 0)
#define EMIT_CLEAR(addr)		((OPCODE_CLEAR << 4) | 1), addr
#define EMIT_WRITE(addr,val)		((OPCODE_WRITE << 4) | 2), addr, val
#define EMIT_MASKWRITE(addr,mask,val)	((OPCODE_MASKWRITE << 4) | 3), addr, mask, val
#define EMIT_MASKPOLL(addr,mask)	((OPCODE_MASKPOLL  << 4) | 2), addr, mask
#define EMIT_MASKDELAY(addr,mask)	((OPCODE_MASKDELAY << 4) | 2), addr, mask

#define PS7_INIT_SUCCESS		(0) //0 is success in good old C
#define PS7_INIT_CORRUPT		(1) // 1 the data is corrupted, and slcr reg are in corrupted state now
#define PS7_INIT_TIMEOUT		(2) // 2 when a poll operation timed out
#define PS7_POLL_FAILED_DDR_INIT	(3) // 3 when a poll operation timed out for ddr init
#define PS7_POLL_FAILED_DMA		(4) // 4 when a poll operation timed out for dma done bit
#define PS7_POLL_FAILED_PLL		(5) // 5 when a poll operation timed out for pll sequence init

static unsigned long ps7_mio_nand_3_0[] = {
	EMIT_MASKWRITE(0XF8000008, 0x0000FFFF, 0x0000DF0D),
	EMIT_MASKWRITE(0xF8000700, 0x00003FFF, 0x00000610),
	EMIT_MASKWRITE(0xF8000708, 0x00003FFF, 0x00000610),
	EMIT_MASKWRITE(0xF800070c, 0x00003FFF, 0x00000610),
	EMIT_MASKWRITE(0xF8000710, 0x00003FFF, 0x00000610),
	EMIT_MASKWRITE(0xF8000714, 0x00003FFF, 0x00000610),
	EMIT_MASKWRITE(0xF8000718, 0x00003FFF, 0x00000610),
	EMIT_MASKWRITE(0xF800071c, 0x00003FFF, 0x00000610),
	EMIT_MASKWRITE(0xF8000720, 0x00003FFF, 0x00000610),
	EMIT_MASKWRITE(0xF8000724, 0x00003FFF, 0x00001610),
	EMIT_MASKWRITE(0xF8000728, 0x00003FFF, 0x00001610),
	EMIT_MASKWRITE(0xF800072c, 0x00003FFF, 0x00001610),
	EMIT_MASKWRITE(0xF8000730, 0x00003FFF, 0x00001610),
	EMIT_MASKWRITE(0xF8000734, 0x00003FFF, 0x00001610),
	EMIT_MASKWRITE(0xF8000738, 0x00003FFF, 0x00001610),
	EMIT_MASKWRITE(0XF8000004, 0x0000FFFF, 0x0000767B),

	EMIT_EXIT(),
};

static unsigned long ps7_mio_qspi_3_0[] = {
	EMIT_MASKWRITE(0XF8000008, 0x0000FFFF, 0x0000DF0D), //unlock

	EMIT_MASKWRITE(0xF8000700, 0x00003FFF, 0x00001601),
	EMIT_MASKWRITE(0XF8000004, 0x0000FFFF, 0x00001602),
	EMIT_MASKWRITE(0xF8000708, 0x00003FFF, 0x00000602),
	EMIT_MASKWRITE(0xF800070c, 0x00003FFF, 0x00000602),
	EMIT_MASKWRITE(0xF8000710, 0x00003FFF, 0x00000602),
	EMIT_MASKWRITE(0xF8000714, 0x00003FFF, 0x00000602),
	EMIT_MASKWRITE(0xF8000718, 0x00003FFF, 0x00000602),
	EMIT_MASKWRITE(0xF800071c, 0x00003FFF, 0x00000610),

	EMIT_MASKWRITE(0XF8000004, 0x0000FFFF, 0x0000767B), //lock

	EMIT_EXIT(),
};

static unsigned long *ps7_mio_init_data_4nand = ps7_mio_nand_3_0;
static unsigned long *ps7_mio_init_data_4qspi = ps7_mio_qspi_3_0;

static int zynq_mio_config(unsigned long *ps7_config_init)
{
	unsigned long *ptr = ps7_config_init;

	unsigned long opcode;   // current instruction ..
	unsigned long args[16]; // no opcode has so many args
	int numargs;    // number of arguments of this instruction
	int j;  // general purpose index

	volatile unsigned long *addr;    // some variable to make code readable
	unsigned long  val,mask;              // some variable to make code readable

	int finish = -1 ;           // loop while this is negative !
	int i = 0;                  // Timeout variable
	//int delay;

	while (finish < 0) {
		numargs = ptr[0] & 0xF;
		opcode = ptr[0] >> 4;

		for (j = 0; j < numargs; j++)
			args[j] = ptr[j+1];
		ptr += numargs + 1;

		switch (opcode) {
		case OPCODE_EXIT:
			finish = PS7_INIT_SUCCESS;
			break;
		case OPCODE_CLEAR:
			addr = (unsigned long*)args[0];
			*addr = 0;
			break;
		case OPCODE_WRITE:
			addr = (unsigned long*)args[0];
			val = args[1];
			*addr = val;
			break;
		case OPCODE_MASKWRITE:
			addr = (unsigned long*)args[0];
			mask = args[1];
			val = args[2];
			*addr = (val & mask) | (*addr & ~mask);
			break;
		case OPCODE_MASKPOLL:
			addr = (unsigned long*) args[0];
			mask = args[1];

			i = 0;
			while (!(*addr & mask)) {
				if (i == PS7_MASK_POLL_TIME) {
					finish = PS7_INIT_TIMEOUT;
					break;
				}
				i++;
			}
			break;
		default:
			finish = PS7_INIT_CORRUPT;
			break;
		}
	}

	return finish;
}

int set_current_storage_to_nand(void)
{
	return zynq_mio_config(ps7_mio_init_data_4nand);
}

int set_current_storage_to_qspi(void)
{
	return zynq_mio_config(ps7_mio_init_data_4qspi);
}
