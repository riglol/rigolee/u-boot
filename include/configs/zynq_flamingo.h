/*
 * (C) Copyright 2013 Xilinx, Inc.
 *
 * Configuration settings for the Xilinx Zynq ZC702 and ZC706 boards
 * See zynq-common.h for Zynq common configs
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __CONFIG_ZYNQ_FLAMINGO_H
#define __CONFIG_ZYNQ_FLAMINGO_H

#define CONFIG_CPLD_TEST
#define CONFIG_SYS_SDRAM_SIZE		(0x1C000000) //( 432 * 1024 * 1024 )
#define CONFIG_ZYNQ_SERIAL_UART0
#define CONFIG_PHY_MICREL_KSZ9031
#define CONFIG_ZYNQ_GEM0
#define CONFIG_ZYNQ_GEM_PHY_ADDR0	4
#define CONFIG_GEM0_EMIO		0
/* save the environment variables to nand block 0 */
#define CONFIG_SYS_NO_FLASH
/* #define CONFIG_ENV_IS_NOWHERE */  /* Do not allow to save the environment variables */

#define CONFIG_ZYNQ_USB
#define CONFIG_ZYNQ_QSPI
#define CONFIG_ZYNQ_I2C
#define CONFIG_ZYNQ_SPI
#define CONFIG_CFB_CONSOLE
#define CONFIG_ZYNQ_VIDEO
//#define CONFIG_USE_IRQ
#define CONFIG_NAND_ZYNQ
#define CONFIG_CMD_ZYNQ_RSA

#define CONFIG_DEFAULT_DEVICE_TREE	zynq-flamingo

#define CONFIG_GENERIC_DISPLAY

#define CONFIG_FDT_BLOB			0x02000000
#include <configs/zynq-common.h>

#endif /* __CONFIG_ZYNQ_FLAMINGO_H */
