#include <common.h>
#include <command.h>
#include <spi.h>

#include "cmd_cpld.h"
#include "keyled.h"

static int spi_flag = (SPI_XFER_BEGIN | SPI_XFER_END);

static int spi_open(int cs_sel)
{
	struct spi_slave *slave;
	int mode        = SPI_MODE_0;
	int bus         = SPI_BUS;
	int cs          = cs_sel;
	int spi_max_hz  = SPI_SPEED;

	slave = spi_setup_slave(bus, cs, spi_max_hz, mode);
	if (!slave) {
		printf("Invalid device %d:%d\n", bus, cs);
		return 0;
	}

	spi_claim_bus(slave);

	return (int)slave;
}

static int spi_close(int slave)
{
	if (!slave)
		return -1;

	spi_release_bus((struct spi_slave*)slave);
	spi_free_slave((struct spi_slave*)slave);

	return 0;
}

static int spi_write(int slave, const void *buf, int bufsize)
{
	int ret = 0;

	if ((!slave) || (bufsize <= 0))
		return 0;

	if (spi_xfer((struct spi_slave*)slave, (bufsize << 3), buf, NULL, spi_flag) != 0)
		printf("Error during SPI transaction\n");
	else
		ret = bufsize;

	return ret;
}

static int spi_read(int slave, void *buf, int bufsize)
{
	int ret = 0;

	if ((!slave) || (bufsize <= 0))
		return 0;

	if (spi_xfer((struct spi_slave*)slave, (bufsize << 3), buf, buf, spi_flag) != 0)
		printf("Error during SPI transaction\n");
	else
		ret = bufsize;

	return ret;
}

static int spi2cpld(int cmd, short val)
{
	int spi = spi_open(SPI_CS_CPLD);

	if (spi) {
		u8 au8Data[3] = { 0, 0, 0 };
		au8Data[0] = cmd;
		au8Data[1] = (val >> 8) & 0xff;
		au8Data[2] = val & 0xff;
		spi_write(spi, au8Data, sizeof(au8Data));
		spi_close(spi);

		return 0;
	}

	return -1;
}

static int cpld2spi(int cmd, short* val)
{
	int spi = spi_open(SPI_CS_CPLD);

	if (spi) {
		u8 au8Data[3] = { 0, 0, 0 };
		au8Data[0] = cmd;
		spi_read(spi, au8Data, sizeof(au8Data));
		*val = (au8Data[1] << 8) | au8Data[2];
		spi_close(spi);

		return 0;
	}

	return -1;
}

static int do_beeper(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int para = 0;

	if (argc == 2) {
		para = simple_strtoul(argv[1], NULL, 10);
		if (para < 4) {
			spi2cpld(CFG_BEEPER, para);
			return 0;
		}
	}

	spi2cpld(CFG_BEEPER, BEEPER_ONCE);

	return 0;
}

static int do_cpldver(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int ret = 0;

	cpld2spi(CPLD_VERSION, (short*)&ret);

	printf("cpld version is: 0x%02x\n", ret);

	return 0;
}

static int do_hwver(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int   ret = 0;

	cpld2spi(SP_VERSION, (short*)&ret);

	printf("HW version is: 0x%04x\n", ret);

	return 0;
}

static int do_dver(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	extern int zynq_lcd_init(void);

	zynq_lcd_init();

	return 0;
}

static int do_restart(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	spi2cpld(CFG_POWER, POWER_RESTART);

	return 0;
}

int do_reset(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	do_restart(cmdtp, flag, argc, argv);

	return 0;
}

/*
0 = close
1 = once
2 = interval
3 = continous
*/
void beeper(int cmd)
{
	int value = BEEPER_OFF;

	if (cmd == 1)
		value = BEEPER_ONCE;
	else if(cmd == 2)
		value = BEEPER_INTERVAL;
	else if(cmd == 3)
		value = BEEPER_CONTINUE;

	spi2cpld(CFG_BEEPER, value);
}

/* 0 = finger 1 = onboard */
int detect_gold_finger(void)
{
	int ret = 1;

	if (0 == cpld2spi(CFG_QSPI_CS, (short*)&ret))
		return ret;

	return 1;
}

/*-------------------------------
 0 = finger 1 = onboard
1. set
2. check whether it is set OK
---------------------------------*/
int set_boot_from(int from)
{
	int ret = 2;

	spi2cpld(CFG_QSPI_CS, from);
	udelay(200000); //200ms
	cpld2spi(CFG_QSPI_CS, (short*)&ret);

	return ret;
}

void lcd_backlight_on(void)
{
	spi2cpld(CFG_BACKLIGHT, BACKLIGHT_ON(50));
}

static int do_goldFinger(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	set_boot_from(GOLD_FINGER);

	return 0;
}

U_BOOT_CMD(
	dver, 1, 0, do_dver,
	"DPU version", ""
);

U_BOOT_CMD(
	hwver, 1, 1, do_hwver,
	"Get hardware version",
	"Get hardware version"
);

U_BOOT_CMD(
	cpldver, 1, 1, do_cpldver,
	"Get cpld version",
	"Get cpld version"
);

U_BOOT_CMD(
	restart, 1, 1, do_restart,
	"Restart the power",
	"Restart the power"
);

U_BOOT_CMD(
	beeper, 2, 1, do_beeper,
	"Beeper",
	"Beeper [0/1/2/3]"
);

U_BOOT_CMD(
	goldFinger, 1, 1, do_goldFinger,
	"Set boot from Gold finger",
	"goldFinger"
);
